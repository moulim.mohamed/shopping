<?php

namespace Drupal\commun\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CategoryMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * CategoryMenuLinkDeriver constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    $terms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadTree('product_category', 0, 2, TRUE);

    $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();

    foreach ($terms as $term) {
      // Get URL of object.
      $term_url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()]);

      // Create menu ID.
      $menuId = 'commun.category.links:' . $term->id();
      
      $title = $term->getName();
      if($term->hasTranslation($language)){
        $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $language);
        $title = $translated_term->getName();
      }
      
      // Create link along with default settings.
      $link = [
        'id' => $menuId,
        'url' => "internal:/product-search/".$term->getName(),
        'title' => $title,
        'weight' => $term->getWeight(),
        'menu_name' => 'main',
      ] + $base_plugin_definition;

      if ($term->parents[0] != 0) {
        // If the parent is not 0 then set the parent attribute of the link.
        $link['parent'] = 'commun.category.links:commun.category.links:' . $term->parents[0];
      }

      // Add link to the bank of links we will return.
      $links[$menuId] = $link;
    }

    return $links;
  }

}