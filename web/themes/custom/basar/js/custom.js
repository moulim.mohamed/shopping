(function ($) {
    "use strict";
    Drupal.behaviors.customBehavior = {
        attach: function (context, settings) {
            $(document).ajaxSuccess(function (e) {
                $('.slider-tabfor').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slider-tabnav'
                });
                $('.slider-tabnav').slick({
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    asNavFor: '.slider-tabfor',
                    dots: false,
                    arrows: true,
                    focusOnSelect: true,
                    centerMode: true,
                    centerPadding: 0
                });
            })
            
            if ($('#slider-range', context).length) {
                let min = settings.facets.rangeInput.product_price.currentValues ? settings.facets.rangeInput.product_price.currentValues.minimum : 10;
                let max = settings.facets.rangeInput.product_price.currentValues ? settings.facets.rangeInput.product_price.currentValues.maximum : 2000;
                $("#slider-range").slider({
                    range: true,
                    step: 5,
                    min: 0,
                    max: 1000
                });
                
                $("#slider-range").slider('option', 'values', [min, max]);

                $("#amount").html("€ " + min + " - €" + max);

                $("#slider-range").slider({
                    slide: function (event, ui) {
                        $("#amount").html("€ " + ui.values[0] + " - € " + ui.values[1]);
                    },
                    stop: function (event, ui) { 
                        $('#product_price_minimum').val(ui.values[0]);
                        $('#product_price_maximum').val(ui.values[1]);
                        $('#facets-range-input-form button').trigger('click');
                    }
                });
            }
            $('.search-widget input[name="search_api_fulltext"]').removeAttr('class');
        }
    }
})(jQuery);