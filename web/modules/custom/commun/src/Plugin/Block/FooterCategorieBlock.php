<?php

namespace Drupal\commun\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "footer_categories_block",
 *   admin_label = @Translation("Categories"),
 *   category = "Footer_categories"
 * )
 */
class FooterCategorieBlock extends BlockBase implements ContainerFactoryPluginInterface
{

    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManager
     */
    protected $entityTypeManager;

    /**
     * CategoryMenuLinkDeriver constructor.
     *
     * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
     *   The entity type manager.
     */
    public function __construct(EntityTypeManager $entityTypeManager, array $configuration, $plugin_id, $plugin_definition)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->entityTypeManager = $entityTypeManager;
    }

    /**
     * Summary of create
     * @param ContainerInterface $container
     * @return static
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $container->get('entity_type.manager'),
            $configuration,
            $plugin_id,
            $plugin_definition
        );
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $url_catalog_view = Url::fromRoute('view.product_search.page_1')->toString();
        
        $terms = $this->entityTypeManager
            ->getStorage('taxonomy_term')
            ->loadTree('product_category', 0, 2, TRUE);

        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

        foreach ($terms as $term) {
            if ($term->parents[0] != 0) {
                $title = $term->getName();
                if ($term->hasTranslation($language)) {
                    $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $language);
                    $title = $translated_term->getName();
                }

                $links[] = [
                    'url' => $url_catalog_view."/" . $term->getName(),
                    'title' => $title
                ];
            }
        }

        return [
            'links' => $links,
            '#theme' => 'footer_categories_block'
        ];
    }
}