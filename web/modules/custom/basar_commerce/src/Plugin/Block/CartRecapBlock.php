<?php
/**
 * @file
 * Contains Drupal\formation_plugin\Plugin\Block\CurrentUser.
 */

namespace Drupal\commun\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Cart Recap' block.
 *
 * @Block(
 *  id = "cart_recap_block",
 *  admin_label = @Translation("Recap cart")
 * )
 */
class CartRecapBlock extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        // Fetch current user.
        $account = \Drupal::currentUser();
        return [
            '#markup' => t(
                '@welcome @username',
                array(
                    '@welcome' => $this->configuration['welcome_text'],
                    '@username' => $account->getAccountName()
                )
            ),
        ];
    }
}