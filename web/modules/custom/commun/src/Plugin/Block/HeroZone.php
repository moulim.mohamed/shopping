<?php
/**
 * @file
 * Contains Drupal\formation_plugin\Plugin\Block\CurrentUser.
 */

namespace Drupal\commun\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'HeroZone' block.
 *
 * @Block(
 *  id = "hero_zone",
 *  admin_label = @Translation("Hero Zone")
 * )
 */
class HeroZone extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    // Fetch current user.
    $account = \Drupal::currentUser();
    return [
      '#markup' => t(
        '@welcome @username',
        array(
          '@welcome' => $this->configuration['welcome_text'],
          '@username' => $account->getAccountName()
        )
      ),
    ];
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form['welcome_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Welcome text'),
      '#description' => $this->t(''),
      '#default_value' => $this->configuration['welcome_text'] ?? '',
    ];
    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['welcome_text'] = $form_state->getValue('welcome_text');
  }
}