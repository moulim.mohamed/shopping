<?php

namespace Drupal\commun\EventSubscriber;

use Drupal\language\ConfigurableLanguageManagerInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use \Drupal\Core\Menu\MenuLinkManagerInterface;


/**
 * Sets the $request property on the language manager.
 */
class LanguageRequestSubscriber implements EventSubscriberInterface
{


  /**
   * The language manager service.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The menu link plugin manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * @param ConfigurableLanguageManagerInterface $language_manager
   * @param MenuLinkManagerInterface $menu_link_manager
   */
  public function __construct(
    ConfigurableLanguageManagerInterface $language_manager,
    MenuLinkManagerInterface $menu_link_manager
  ) {
    $this->languageManager = $language_manager;
    $this->menuLinkManager = $menu_link_manager;
  }

  /**
   * Initializes the language manager at the beginning of the request.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function onKernelRequestLanguage(RequestEvent $event)
  {
    $tempstore = \Drupal::service('tempstore.private');
    $store = $tempstore->get('commun_collection');
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    if ($store->get('language_code') !== $langcode) {
      \Drupal::cache('menu')->invalidateAll();
      $this->menuLinkManager->rebuild();
    }

    $store->set('language_code', $langcode);
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents(): array
  {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestLanguage', 1];

    return $events;
  }

}